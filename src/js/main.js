$.fn.setCursorPosition = function (pos) {
  if ($(this).get(0).setSelectionRange) {
    $(this).get(0).setSelectionRange(pos, pos);
  } else if ($(this).get(0).createTextRange) {
    var range = $(this).get(0).createTextRange();
    range.collapse(true);
    range.moveEnd('character', pos);
    range.moveStart('character', pos);
    range.select();
  }
};

$(document).ready(function () {
  $.validator.addMethod("minlenghtphone", function (value, element) {
      return value.replace(/\D+/g, '').length > 10;
    },
    "Не хватает цифр в номере телефона");
  $.validator.addMethod("requiredphone", function (value, element) {
      return value.replace(/\D+/g, '').length > 1;
    },
    "Заполните это поле");

  $("form").each(function () {
    $(this).validate({
      rules: {
        tel: {
          requiredphone: true,
          minlenghtphone: true
        }
      }
    })
  })

  $("input[type=tel]").click(function () {
    $(this).setCursorPosition(3);
  }).mask("+7 (999) 999 99 99", {
    autoclear: false,
  })


  $('.input_upload').change(function () {
    if ($(this).val() != '') $('.file_label span').text('Загружено: ' + $(this)[0].files.length);
    else $('.file_label span').text('Загрузить');
  });
})
$(document).ready(function () {
  
  $('.js_gallery_4').each(function () {
    $(this).slick({
      dots: false,
      infinite: true,
      speed: 500,
      autoplay: false,
      autoplaySpeed: 2000,
      fade: false,
      arrows: true,
      rows: 2,
      slidesToShow: 4,
      slidesToScroll: 1,
      prevArrow: $(this).parents('.gallery_4').find('.nav_prev'),
      nextArrow: $(this).parents('.gallery_4').find('.nav_next'),
      customPaging: function (slider, i) {
        return '';
      },
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3
          }
    },
        {
          breakpoint: 515,
          settings: {
            slidesToShow: 2
          }
    }
  ]
    })
    
  })
})
$(document).ready(function () {
  $('.js_gallery').magnificPopup({
    delegate: 'a.js_gallery_item',
    type: 'image',
    removalDelay: 500, //delay removal by X to allow out-animation
    tLoading: 'Загрузка изображения...',
    tClose: 'Закрыть (Esc)',
    mainClass: 'mfp-img-mobile',
    fixedContentPos: false,
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      tPrev: 'Предыдущее фото',
      tNext: 'Следующее фото',
      tCounter: 'Фото %curr% из %total%',
      preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
    },
    callbacks: {
      beforeOpen: function () {
        // just a hack that adds mfp-anim class to markup 
        this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
        this.st.mainClass = 'mfp-zoom-in';
      }
    },
    image: {
      tError: 'Не удалось загрузить <a href="%url%">фотографию #%curr%</a>'
    },
    ajax: {
      tError: 'Не удалось загрузить <a href="%url%">фотографию #%curr%</a>.'
    }
  });
});
$(document).ready(function () {

  $('.js_slider_1').each(function () {
    $(this).slick({
      dots: true,
      infinite: true,
      speed: 500,
      autoplay: false,
      autoplaySpeed: 2000,
      arrows: false,
      slidesToShow: 4,
      slidesToScroll: 1,
      appendDots: $(this).parents('.slider_1').find('.rev_dots__slim'),
      dotsClass: 'custom-dots',
      customPaging: function (slider, i) {
        return '';
      },
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3
          }
    },
        {
          breakpoint: 515,
          settings: {
            slidesToShow: 2
          }
    }
  ]
    })
  })
})
$(document).ready(function () {

  $('.js_slider_2').each(function () {
    $(this).slick({
      dots: true,
      infinite: true,
      speed: 500,
      autoplay: false,
      autoplaySpeed: 2000,
      arrows: false,
      slidesToShow: 2,
      slidesToScroll: 1,
      centerMode: false,
      variableWidth: false,
      appendDots: $(this).parents('.slider_2').find('.rev_dots__slim'),
      dotsClass: 'custom-dots',
      customPaging: function (slider, i) {
        return '';
      },
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1
          }
    },
        {
          breakpoint: 515,
          settings: {
            slidesToShow: 1
          }
    }
  ]
    })
  })
})
/* Мобильное меню
—---------------------------------------------------— */
$('.js_menu_open').click(function () {
  $('.mobile_menu').addClass('nav_open');
  $('body').addClass('form_open');
  return false;
});


$('.menu_close').click(function () {
  $('.mobile_menu').removeClass('nav_open');
  $('body').removeClass('form_open');
  return false;
});

$('.mobile_nav li a').click(function () {
  $('.mobile_menu').removeClass('nav_open');
  $('body').removeClass('form_open');
});

$(function () {
  $(document).click(function (event) {
    if ($('.mobile_menu').hasClass('nav_open')) {
      if ($(event.target).closest('.mobile_menu').length) {
        return;
      }
      $('.mobile_menu').removeClass('nav_open');
      $('body').removeClass('form_open');
      event.stopPropagation();
    }
  });
});

/* —---------------------------------------------------— */

/* Аккордеон в мобильном меню
—---------------------------------------------------— */
$('.mobile_nav .menu-item-has-children > a, .mobile_list_page .menu-item-has-children > a').append('<span class="menu_arrow"><svg xmlns="http://www.w3.org/2000/svg" width="10" height="8.182" viewBox="0 0 10 8.182"> <path id="XMLID_308_" d="M30.234,9.943A.455.455,0,0,0,30.7,9.93l7.273-4.545a.455.455,0,0,0,0-.771L30.7.069a.455.455,0,0,0-.7.385V9.545A.455.455,0,0,0,30.234,9.943Z" transform="translate(10 -30) rotate(90)" /> </svg> </span>');

$('.mobile_nav .current-menu-item').addClass('active');

$('.mobile_nav .menu-item-has-children .menu_arrow').on('click', function (e) {
  e.preventDefault();
  var $this = $(this).parents('.menu-item-has-children');

  if (!$this.hasClass('active')) {
    $('.mobile_nav .sub-menu').slideUp();
    $('.mobile_nav .menu-item-has-children').removeClass('active');
  }

  $this.toggleClass('active');
  $this.find('.sub-menu').slideToggle();

  return false;
});
/* —---------------------------------------------------— */
$(document).ready(function() {
	$('.js_open_modal').magnificPopup({
		type: 'inline',
        removalDelay: 300, //delay removal by X to allow out-animation
        callbacks: {
            beforeOpen: function() {
               this.st.mainClass = 'mfp-zoom-in';
            }
          },
	});
});
$('.tabs_ques').click(function () {
  $(this).parents('.tabs_item').toggleClass('active');
  $(this).parents('.tabs_item').find('.tabs_answer').slideToggle(500);
});