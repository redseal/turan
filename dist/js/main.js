$.fn.setCursorPosition = function (pos) {
  if ($(this).get(0).setSelectionRange) {
    $(this).get(0).setSelectionRange(pos, pos);
  } else if ($(this).get(0).createTextRange) {
    var range = $(this).get(0).createTextRange();
    range.collapse(true);
    range.moveEnd('character', pos);
    range.moveStart('character', pos);
    range.select();
  }
};

$(document).ready(function () {
  $.validator.addMethod("minlenghtphone", function (value, element) {
      return value.replace(/\D+/g, '').length > 10;
    },
    "Не хватает цифр в номере телефона");
  $.validator.addMethod("requiredphone", function (value, element) {
      return value.replace(/\D+/g, '').length > 1;
    },
    "Это поле необходимо заполнить.");

  $("form").each(function () {
    $(this).validate({
      rules: {
        tel: {
          requiredphone: true,
          minlenghtphone: true
        }
      }
    })
  })

  $("input[type=tel]").click(function () {
    $(this).setCursorPosition(3);
  }).mask("+7 (999) 999 99 99", {
    autoclear: false,
  })


  $('.input_upload').change(function () {
    if ($(this).val() != '') $('.file_label span').text('Загружено: ' + $(this)[0].files.length);
    else $('.file_label span').text('Загрузить');
  });

  $('.home_slider').slick({
    dots: false,
    infinite: true,
    slidesToShow: 1,
    speed: 300,
    fade: true,
    draggable: true,
    cssEase: 'linear',
    prevArrow: $('.home_slider__arrows .prev'),
    nextArrow: $('.home_slider__arrows .next')

  });

  $('.about_nav_slider').slick({
    dots: false,
    infinite: false,
    slidesToShow: 3,
    rows: 3,
    prevArrow: $('.home_nav_section .prev'),
    nextArrow: $('.home_nav_section .next'),
    responsive: [
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 560,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  $('.gallery_slider').slick({
    dots: false,
    infinite: true,
    slidesToShow: 3,
    prevArrow: $('.home_gallery_slider .prev'),
    nextArrow: $('.home_gallery_slider .next'),
    responsive: [
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 560,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  $('.news_slider').slick({
    dots: false,
    infinite: true,
    slidesToShow: 4,
    prevArrow: $('.home_news_slider .prev'),
    nextArrow: $('.home_news_slider .next'),
    responsive: [
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 650,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 560,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  $('.partner_slider').slick({
    dots: false,
    infinite: true,
    rows:2,
    slidesToShow: 4,
    prevArrow: $('.partner_section .prev'),
    nextArrow: $('.partner_section .next'),
    responsive: [
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2
        }
      }
    ]
  });

  $('.js_gallery').magnificPopup({
    delegate: 'a.js_gallery_item',
    type: 'image',
    removalDelay: 500, //delay removal by X to allow out-animation
    tLoading: 'Загрузка изображения...',
    tClose: 'Закрыть (Esc)',
    mainClass: 'mfp-img-mobile',
    fixedContentPos: false,
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      tPrev: 'Предыдущее фото',
      tNext: 'Следующее фото',
      tCounter: 'Фото %curr% из %total%',
      preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
    },
    callbacks: {
      beforeOpen: function () {
        // just a hack that adds mfp-anim class to markup 
        this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
        this.st.mainClass = 'mfp-zoom-in';
      }
    },
    image: {
      tError: 'Не удалось загрузить <a href="%url%">фотографию #%curr%</a>'
    },
    ajax: {
      tError: 'Не удалось загрузить <a href="%url%">фотографию #%curr%</a>.'
    }
  });
});

/* Мобильное меню
—---------------------------------------------------— */
$('.js_menu_open').click(function () {
  $('.mobile_menu').addClass('nav_open');
  $('body').addClass('form_open');
  return false;
});


$('.menu_close').click(function () {
  $('.mobile_menu').removeClass('nav_open');
  $('body').removeClass('form_open');
  return false;
});

$('.mobile_nav li a').click(function () {
  $('.mobile_menu').removeClass('nav_open');
  $('body').removeClass('form_open');
});

$(function () {
  $(document).click(function (event) {
    if ($('.mobile_menu').hasClass('nav_open')) {
      if ($(event.target).closest('.mobile_menu').length) {
        return;
      }
      $('.mobile_menu').removeClass('nav_open');
      $('body').removeClass('form_open');
      event.stopPropagation();
    }
  });
});

/* —---------------------------------------------------— */

/* Аккордеон в мобильном меню
—---------------------------------------------------— */
$('.mobile_nav .menu-item-has-children > a, .mobile_list_page .menu-item-has-children > a').append('<span class="menu_arrow"><svg xmlns="http://www.w3.org/2000/svg" width="19.864" height="12.521" viewBox="0 0 19.864 12.521"><g id="right-chevron" transform="translate(19.864) rotate(90)"><path id="Контур_3" data-name="Контур 3" d="M12.048,8.784,3.736.484A1.515,1.515,0,0,0,2.589,0,1.516,1.516,0,0,0,1.441.484l-.969.956A1.561,1.561,0,0,0,0,2.588a1.632,1.632,0,0,0,.472,1.16l6.2,6.184-6.2,6.2A1.562,1.562,0,0,0,0,17.276a1.632,1.632,0,0,0,.472,1.16l.969.956a1.562,1.562,0,0,0,1.148.472,1.562,1.562,0,0,0,1.148-.472l8.313-8.3a1.633,1.633,0,0,0,.472-1.16A1.562,1.562,0,0,0,12.048,8.784Z" transform="translate(0 0)" fill="#08c"/></g></svg></span>');

$('.mobile_nav .current-menu-item').addClass('active');

$('.mobile_nav .menu-item-has-children .menu_arrow').on('click', function (e) {
  e.preventDefault();
  var $this = $(this).parents('.menu-item-has-children');

  if (!$this.hasClass('active')) {
    $('.mobile_nav .sub-menu').slideUp();
    $('.mobile_nav .menu-item-has-children').removeClass('active');
  }

  $this.toggleClass('active');
  $this.find('.sub-menu').slideToggle();

  return false;
});
/* —---------------------------------------------------— */
$(document).ready(function() {
	$('.js_open_modal').magnificPopup({
		type: 'inline',
        removalDelay: 300, //delay removal by X to allow out-animation
        callbacks: {
            beforeOpen: function() {
               this.st.mainClass = 'mfp-zoom-in';
            }
          },
	});
});
$('.tabs_ques').click(function () {
  $(this).parents('.tabs_item').toggleClass('active');
  $(this).parents('.tabs_item').find('.tabs_answer').slideToggle(500);
});


// /* Select
// —---------------------------------------------------— */
// const selectSingle = document.querySelector('.lang__select');
// const selectSingle_title = selectSingle.querySelector('.lang__select_title');
// const selectSingle_labels = selectSingle.querySelectorAll('.lang__select_label');

// // Toggle menu
// selectSingle_title.addEventListener('click', () => {
//   if ('active' === selectSingle.getAttribute('data-state')) {
//     selectSingle.setAttribute('data-state', '');
//   } else {
//     selectSingle.setAttribute('data-state', 'active');
//   }
// });

// // Close when click to option
// for (let i = 0; i < selectSingle_labels.length; i++) {
//   selectSingle_labels[i].addEventListener('click', (evt) => {
//     selectSingle_title.textContent = evt.target.textContent;
//     selectSingle.setAttribute('data-state', '');
//   });
// }

